-- What extra information can I add to the model?
-- Marketing engagement per account within the year, maybe MQLs?
-- # of created opportunities within previous year
-- product currently purchased
-- # of seats at that point in time
-- won IACV previous year
-- lost IACV previous year - non renewal
-- lost renewal previous year

[nf_ptb_account_features]
SELECT dim_parent_crm_account_id,
      parent_crm_account_sales_segment,
      current_month               AS target_month,
    
      current_arr                 AS target_arr,                
      minus_1p_arr                AS current_arr,
      minus_2p_arr                AS minus_1p_arr,
      minus_3p_arr                AS minus_2p_arr,

      current_created_opties      AS target_created_opties,
      minus_1_created_opties      AS current_created_opties,
      minus_2_created_opties      AS minus_1_created_opties,
      minus_3_created_opties      AS minus_2_created_opties,
   
      current_won_opties          AS target_won_opties,
      minus_1_won_opties          AS current_won_opties,
      minus_2_won_opties          AS minus_1_won_opties,
   
      current_lost_opties         AS target_lost_opties,
      minus_1_lost_opties         AS current_lost_opties,
      minus_2_lost_opties         AS minus_1_lost_opties,

      current_created_contacts    AS target_created_contacts,
      minus_1_created_contacts    AS current_created_contacts,
      minus_2_created_contacts    AS minus_1_created_contacts,
      minus_3_created_contacts    AS minus_2_created_contacts,

      current_created_support_tickets  AS target_created_support_tickets,
      minus_1_created_support_tickets  AS current_created_support_tickets,
      minus_2_created_support_tickets  AS minus_1_created_support_tickets,
      minus_3_created_support_tickets  AS minus_2_created_support_tickets,

      current_sfdc_tasks    AS target_sfdc_tasks,
      minus_1_sfdc_tasks    AS current_sfdc_tasks,
      minus_2_sfdc_tasks    AS minus_1_sfdc_tasks,
      minus_3_sfdc_tasks    AS minus_2_sfdc_tasks,

      current_biz_touchpoints  AS target_biz_touchpoints,
      minus_1_biz_touchpoints  AS current_biz_touchpoints,
      minus_2_biz_touchpoints  AS minus_1_biz_touchpoints,
      minus_3_biz_touchpoints  AS minus_2_biz_touchpoints,

      current_ping_count AS target_ping_count,
      minus_1_ping_count  AS current_ping_count,
      minus_2_ping_count  AS minus_1_ping_count,
      minus_3_ping_count  AS minus_2_ping_count,
      
      current_avg_instance_user_count  AS target_avg_instance_user_count,
      minus_1_avg_instance_user_count  AS current_avg_instance_user_count,
      minus_2_avg_instance_user_count  AS minus_1_avg_instance_user_count,
      minus_3_avg_instance_user_count  AS minus_2_avg_instance_user_count,
   
    -- MV data fields
     -- current_delta_arr                  AS target_delta_arr,
     -- minus_1_delta_arr                  AS current_delta_arr,
     -- minus_2_delta_arr                  AS minus_1_delta_arr,
     -- minus_3_delta_arr                  AS minus_2_delta_arr,

      current_seat_change_arr      AS target_seat_change_arr,
      minus_1_seat_change_arr      AS current_seat_change_arr,
      minus_2_seat_change_arr      AS minus_1_seat_change_arr,
      minus_3_seat_change_arr      AS minus_2_seat_change_arr,

      current_price_change_arr      AS target_price_change_arr,
      minus_1_price_change_arr      AS current_price_change_arr,
      minus_2_price_change_arr      AS minus_1_price_change_arr,
      minus_3_price_change_arr      AS minus_2_price_change_arr,

      current_tier_change_arr      AS target_tier_change_arr,
      minus_1_tier_change_arr      AS current_tier_change_arr,
      minus_2_tier_change_arr      AS minus_1_tier_change_arr,
      minus_3_tier_change_arr      AS minus_2_tier_change_arr,

      current_number_seats      AS target_number_seats,
      minus_1_number_seats      AS current_number_seats,
      minus_2_number_seats      AS minus_1_number_seats,
      minus_3_number_seats      AS minus_2_number_seats,
 

      current_product      AS target_product,
      minus_1_product      AS current_product,
      minus_2_product      AS minus_1_product,
      minus_3_product      AS minus_2_product,

      delivery,
      upa_industry

FROM arr_per_periods
WHERE ultimate_parent_account_segment IN ('Large','Mid-Market')
AND (minus_1p_arr > 0 OR minus_2p_arr > 0 OR minus_3p_arr > 0)
UNION
SELECT ultimate_parent_account_id,
      ultimate_parent_account_segment,
      minus_1_year_month              AS target_month,

      minus_1p_arr                    AS target_arr,
      minus_2p_arr                    AS minus_1p_arr,
      minus_3p_arr                    AS minus_2p_arr,
      minus_4p_arr                    AS minus_3p_arr,

      minus_1_created_opties          AS target_created_opties,
      minus_2_created_opties          AS current_created_opties,
      minus_3_created_opties          AS minus_1_created_opties,
      minus_4_created_opties          AS minus_2_created_opties,
    
      minus_1_won_opties              AS target_won_opties,
      minus_2_won_opties              AS current_won_opties,
      minus_3_won_opties              AS minus_1_won_opties,
   
      minus_1_lost_opties             AS target_lost_opties,
      minus_2_lost_opties             AS current_lost_opties,
      minus_3_lost_opties             AS minus_1_lost_opties,

      minus_1_created_contacts    AS target_created_contacts,
      minus_2_created_contacts    AS current_created_contacts,
      minus_3_created_contacts    AS minus_1_created_contacts,
      minus_4_created_contacts    AS minus_2_created_contacts,

      minus_1_created_support_tickets  AS target_created_support_tickets,
      minus_2_created_support_tickets  AS current_created_support_tickets,
      minus_3_created_support_tickets  AS minus_1_created_support_tickets,
      minus_4_created_support_tickets  AS minus_2_created_support_tickets,

      minus_1_sfdc_tasks    AS target_sfdc_tasks,
      minus_2_sfdc_tasks    AS current_sfdc_tasks,
      minus_3_sfdc_tasks    AS minus_1_sfdc_tasks,
      minus_4_sfdc_tasks    AS minus_2_sfdc_tasks,

      minus_1_biz_touchpoints    AS target_biz_touchpoints,
      minus_2_biz_touchpoints    AS current_biz_touchpoints,
      minus_3_biz_touchpoints    AS minus_1_biz_touchpoints,
      minus_4_biz_touchpoints    AS minus_2_biz_touchpoints,

      minus_1_ping_count          AS target_ping_count,
      minus_2_ping_count          AS current_ping_count,
      minus_3_ping_count          AS minus_1_ping_count,
      minus_4_ping_count          AS minus_2_ping_count,
      
      minus_1_avg_instance_user_count  AS target_avg_instance_user_count,
      minus_2_avg_instance_user_count  AS current_avg_instance_user_count,
      minus_3_avg_instance_user_count  AS minus_1_avg_instance_user_count,
      minus_4_avg_instance_user_count  AS minus_2_avg_instance_user_count,

      -- MV data fields
      --minus_1_delta_arr            AS target_delta_arr,
      --minus_2_delta_arr            AS current_delta_arr,
     -- minus_3_delta_arr            AS minus_1_delta_arr,
     -- minus_4_delta_arr            AS minus_2_delta_arr,

      minus_1_seat_change_arr      AS target_seat_change_arr,
      minus_2_seat_change_arr      AS current_seat_change_arr,
      minus_3_seat_change_arr      AS minus_1_seat_change_arr,
      minus_4_seat_change_arr      AS minus_2_seat_change_arr,

      minus_1_price_change_arr      AS target_price_change_arr,
      minus_2_price_change_arr      AS current_price_change_arr,
      minus_3_price_change_arr      AS minus_1_price_change_arr,
      minus_4_price_change_arr      AS minus_2_price_change_arr,

      minus_1_tier_change_arr      AS target_tier_change_arr,
      minus_2_tier_change_arr      AS current_tier_change_arr,
      minus_3_tier_change_arr      AS minus_1_tier_change_arr,
      minus_4_tier_change_arr      AS minus_2_tier_change_arr,

      minus_1_number_seats      AS target_number_seats,
      minus_2_number_seats      AS current_number_seats,
      minus_3_number_seats      AS minus_1_number_seats,
      minus_4_number_seats      AS minus_2_number_seats,
 
      minus_1_product           AS target_product,
      minus_2_product           AS current_product,
      minus_3_product           AS minus_1_product,
      minus_4_product           AS minus_2_product,

      delivery,
      upa_industry

FROM arr_per_periods
WHERE parent_crm_account_sales_segment IN ('Large','Mid-Market')
AND (minus_2p_arr > 0 OR minus_3p_arr > 0 OR minus_4p_arr > 0)
