-- NF: The CTEs are called UPA but this query actually work with child account
-- I just didn't change the names when updating the original UPA model
WITH target_periods AS (
  SELECT DISTINCT
      first_day_of_month                       AS current_month,
      DATEADD(year,-1,first_day_of_month)      AS minus_1_year_month,
      DATEADD(year,-2,first_day_of_month)      AS minus_2_year_month,
      DATEADD(year,-3,first_day_of_month)      AS minus_3_year_month,
      DATEADD(year,-4,first_day_of_month)      AS minus_4_year_month
  FROM legacy.DATE_DETAILS 
  WHERE first_day_of_month = dateadd(month, -1, DATE_TRUNC('month',CURRENT_DATE))
), agg_arr_month AS (
  SELECT 
        dim_crm_account_id                                        AS account_id, 
        arr_month,
        MIN(subscription_start_month)                 AS min_subscription_start_date,  
        MAX(subscription_end_month)                   AS max_subscription_end_date,
        count(DISTINCT subscription_name)             AS total_count_subscriptions,
        sum(arr)                                      AS arr
  FROM common_mart_sales.mart_arr
  --WHERE account_id = '0014M00001fc2dJQAQ'
  GROUP BY 1, 2
  
), excluded_upas AS (
  SELECT dim_crm_account_id         AS account_id,
        parent_crm_account_sales_segment,                                           
        ROW_NUMBER() OVER (PARTITION BY parent_crm_account_sales_segment ORDER BY SUM(arr) desc) AS arr_rank
  FROM common_mart_sales.mart_arr
  WHERE arr_month =  DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))
  AND parent_crm_account_sales_segment IN ('Large','Mid-Market')
  GROUP BY 1, 2
), upa AS (
  SELECT DISTINCT a.dim_crm_account_id     AS account_id,
                  a.parent_crm_account_sales_segment,
                  ac.industry           AS upa_industry
       FROM common_mart_sales.mart_arr a
       LEFT JOIN legacy.SFDC_ACCOUNTS_XF ac
          ON ac.account_id = a.dim_crm_account_id
       WHERE a.parent_crm_account_sales_segment IN ('Large','Mid-Market')
 --      AND a.dim_crm_account_id NOT IN (SELECT distinct account_id
 --                                               FROM excluded_upas
 --                                               WHERE arr_rank <= 15)
-----------------------------------------------------------------------
---> SFDC CREATED OPPORTUNITIES
  
), opty_summary AS (
SELECT a.account_id,
  
------------------------
  
    sum(CASE WHEN  o.CREATED_DATE_MONTH 
            BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
               AND  DATEADD(year,-0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
          THEN 1 ELSE 0 END)                                           AS current_created_opties,
    sum(CASE WHEN  o.CREATED_DATE_MONTH 
        BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
      THEN 1 ELSE 0 END)                                               AS minus_1_created_opties,
    sum(CASE WHEN  o.CREATED_DATE_MONTH 
        BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
      THEN 1 ELSE 0 END)                                               AS minus_2_created_opties,
      sum(CASE WHEN  o.CREATED_DATE_MONTH 
        BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
      THEN 1 ELSE 0 END)                                               AS minus_3_created_opties,
      sum(CASE WHEN  o.CREATED_DATE_MONTH 
        BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
      THEN 1 ELSE 0 END)                                               AS minus_4_created_opties,
------------------------    
    sum(CASE WHEN  o.close_date_month 
            BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
               AND  DATEADD(year,-0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
            AND o.is_won = 1
          THEN 1 ELSE 0 END)                                           AS current_won_opties,
    sum(CASE WHEN  o.close_date_month 
        BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))
         AND o.is_won = 1
      THEN 1 ELSE 0 END)                                               AS minus_1_won_opties,
    sum(CASE WHEN  o.close_date_month 
        BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
          AND o.is_won = 1
      THEN 1 ELSE 0 END)                                               AS minus_2_won_opties,
    sum(CASE WHEN  o.close_date_month 
        BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
          AND o.is_won = 1
      THEN 1 ELSE 0 END)                                               AS minus_3_won_opties,
------------------------  
  sum(CASE WHEN  o.close_date_month 
            BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
               AND  DATEADD(year,-0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
            AND o.is_lost = 1
          THEN 1 ELSE 0 END)                                           AS current_lost_opties,
    
   sum(CASE WHEN  o.close_date_month 
        BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))
         AND o.is_lost = 1
      THEN 1 ELSE 0 END)                                               AS minus_1_lost_opties,
    sum(CASE WHEN  o.close_date_month 
        BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
          AND o.is_lost = 1
      THEN 1 ELSE 0 END)                                               AS minus_2_lost_opties,
    sum(CASE WHEN  o.close_date_month 
        BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
           AND  DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
          AND o.is_lost = 1
      THEN 1 ELSE 0 END)                                               AS minus_3_lost_opties   

  FROM legacy.sfdc_opportunity_xf o
  LEFT JOIN legacy.SFDC_ACCOUNTS_XF a
      ON a.account_id = o.account_id
  WHERE o.is_deleted = 0
    AND stage_name NOT IN ('9-Unqualified','10-Duplicate','Unqualified','00-Pre Opportunity','0-Pending Acceptance') 
  GROUP BY 1

-----------------------------------------------------------------------
---> SFDC CREATED CONTACTS

),  contact AS (
SELECT a.account_id,
   d.fiscal_quarter_name_fy             AS created_fiscal_quarter_name,
   d.fiscal_year                        AS created_fiscal_year,
   d.first_day_of_fiscal_quarter        AS created_month_date,
   d.date_actual                        AS created_date
  
FROM legacy.sfdc_contact_xf c
INNER JOIN legacy.date_details d
  ON d.date_actual = c.created_date::date
LEFT JOIN legacy.sfdc_accounts_xf a
  ON a.account_id = c.account_id
WHERE c.is_deleted = 0
), contact_summary AS (
SELECT c.account_id,
   sum(CASE 
            WHEN created_date::date 
            BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                AND DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))           
            THEN 1 ELSE 0 END)                 AS current_created_contacts,
   sum(CASE 
            WHEN created_date::date 
              BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))      
                AND DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))        
            THEN 1 ELSE 0 END)                 AS minus_1_created_contacts,
    sum(CASE 
            WHEN created_date::date 
              BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                AND DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
            THEN 1 ELSE 0 END)                 AS minus_2_created_contacts,
     sum(CASE 
            WHEN created_date::date BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))
            AND DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
            THEN 1 ELSE 0 END)                 AS minus_3_created_contacts,
     sum(CASE 
            WHEN created_date::date BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))
            AND DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
            THEN 1 ELSE 0 END)                 AS minus_4_created_contacts
FROM contact c
GROUP BY 1
HAVING minus_1_created_contacts > 0 
  OR minus_2_created_contacts > 0
  OR minus_3_created_contacts > 0
  OR current_created_contacts > 0

-----------------------------------------------------------------------
---> ZENDESK

), zendesk AS (
SELECT a.account_id,
    d.fiscal_quarter_name_fy        AS created_fiscal_quarter_month,
    d.first_day_of_month            AS created_month,
    d.date_actual                   AS created_date,
    count(*)
  FROM legacy.zendesk_tickets_xf t
  INNER JOIN legacy.sfdc_accounts_xf a  
    ON a.account_id = t.sfdc_account_id
  INNER JOIN legacy.date_details d
    ON d.date_actual = t.ticket_created_at::date
GROUP BY 1, 2, 3, 4
), zendesk_summary AS (
  SELECT account_id,
         sum(CASE 
              WHEN created_date::date 
                BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS current_created_support_tickets,
          sum(CASE 
              WHEN created_date::date 
                BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_1_created_support_tickets,
          sum(CASE 
              WHEN created_date::date 
                BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_2_created_support_tickets,
          sum(CASE 
              WHEN created_date::date 
                BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_3_created_support_tickets,
           sum(CASE 
              WHEN created_date::date 
                BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_4_created_support_tickets
  FROM zendesk
  GROUP BY 1
  HAVING minus_1_created_support_tickets > 0
    OR minus_2_created_support_tickets > 0
    OR minus_3_created_support_tickets > 0
    OR minus_4_created_support_tickets > 0
    OR current_created_support_tickets > 0

-----------------------------------------------------------------------
---> SFDC TASKS

-- NF Added 2020-11-17
), tasks AS (
SELECT a.account_id,
    d.fiscal_quarter_name_fy        AS task_fiscal_quarter_month,
    d.first_day_of_month            AS task_month,
    d.date_actual                   AS task_date,
    COUNT(*)
FROM legacy.sfdc_task t
INNER JOIN legacy.sfdc_accounts_xf a  
    ON a.account_id = t.account_id
INNER JOIN legacy.date_details d
  ON t.task_date::DATE = d.date_actual
GROUP BY 1,2,3,4
), tasks_summary AS (
  SELECT account_id,
         sum(CASE 
              WHEN task_date::date 
                BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS current_sfdc_tasks,
          sum(CASE 
              WHEN task_date::date 
                BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_1_sfdc_tasks,
          sum(CASE 
              WHEN task_date::date 
                BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_2_sfdc_tasks,
          sum(CASE 
              WHEN task_date::date 
                BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_3_sfdc_tasks,
           sum(CASE 
              WHEN task_date::date 
                BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_4_sfdc_tasks
  FROM tasks
  GROUP BY 1
  HAVING minus_1_sfdc_tasks > 0
    OR minus_2_sfdc_tasks > 0
    OR minus_3_sfdc_tasks > 0
    OR minus_4_sfdc_tasks > 0
    OR current_sfdc_tasks > 0
  
-----------------------------------------------------------------------
---> BIZIBLE TOUCH POINTS  

), mkt_touch AS (
SELECT
      a.account_id,
      d.fiscal_quarter_name_fy                  AS touchpoint_fiscal_quarter_name,
      d.date_actual                             AS touchpoint_date,
      d.fiscal_year                             AS touchpoint_fiscal_year,
      d.first_day_of_fiscal_quarter             AS touchpoint_fiscal_quarter_date,
      count(*)
FROM legacy.sfdc_bizible_touchpoint b
INNER JOIN legacy.sfdc_bizible_person p
  ON b.bizible_person_id = p.person_id
INNER JOIN legacy.sfdc_contact_xf c
  ON c.contact_id = p.bizible_contact_id
INNER JOIN legacy.sfdc_accounts_xf a
  ON c.account_id = a.account_id
INNER JOIN legacy.date_details d
  ON d.date_actual = b.bizible_touchpoint_date::date
WHERE b.is_deleted = 0
  AND a.is_deleted = 0
  AND c.is_deleted = 0
GROUP BY 1, 2, 3, 4, 5
), mkt_touch_summary AS (
SELECT account_id,
         sum(CASE 
              WHEN touchpoint_date
                BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS current_biz_touchpoints,
          sum(CASE 
              WHEN touchpoint_date
                BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_1_biz_touchpoints,
          sum(CASE 
              WHEN touchpoint_date
                BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_2_biz_touchpoints,
          sum(CASE 
              WHEN touchpoint_date
                BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_3_biz_touchpoints,
           sum(CASE 
              WHEN touchpoint_date
                BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN 1 ELSE 0 END)                 AS minus_4_biz_touchpoints
  
FROM mkt_touch
GROUP BY 1
HAVING minus_1_biz_touchpoints > 0
    OR minus_2_biz_touchpoints > 0
    OR minus_3_biz_touchpoints > 0
    OR minus_4_biz_touchpoints > 0
    OR current_biz_touchpoints > 0

-----------------------------------------------------------------------
---> PING BY ACCOUNT    

), ping_by_host_quarter AS (
SELECT a.account_id,
    ping.ping_date,
    ping.host_id,
    count(DISTINCT ping.usage_ping_id)    AS sum_ping_count,
    avg(ping.instance_user_count)         AS avg_instance_user_count
FROM legacy.usage_ping_mart ping
INNER JOIN legacy.sfdc_accounts_xf a
  ON a.account_id = ping.dim_crm_account_id
LEFT JOIN legacy.date_details d
  ON d.date_actual = ping.ping_date::date
GROUP BY 1, 2, 3
), ping_summary AS (
SELECT account_id,
        -- count pings
         sum(CASE 
              WHEN ping_date 
                BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN sum_ping_count ELSE 0 END)                 AS current_ping_count,
          sum(CASE 
              WHEN ping_date
                BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN sum_ping_count ELSE 0 END)                 AS minus_1_ping_count,
          sum(CASE 
              WHEN ping_date 
                BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN sum_ping_count ELSE 0 END)                 AS minus_2_ping_count,
          sum(CASE 
              WHEN ping_date
                BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN sum_ping_count ELSE 0 END)                 AS minus_3_ping_count,
           sum(CASE 
              WHEN ping_date
                BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN sum_ping_count ELSE 0 END)                 AS minus_4_ping_count,
         
         -- average instance user count
         avg(CASE 
              WHEN ping_date 
                BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN avg_instance_user_count ELSE NULL END)                 AS current_avg_instance_user_count,
          avg(CASE 
              WHEN ping_date
                BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN avg_instance_user_count ELSE NULL END)                 AS minus_1_avg_instance_user_count,
          avg(CASE 
              WHEN ping_date 
                BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN avg_instance_user_count ELSE NULL END)                 AS minus_2_avg_instance_user_count,
          avg(CASE 
              WHEN ping_date
                BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN avg_instance_user_count ELSE NULL END)                 AS minus_3_avg_instance_user_count,
           avg(CASE 
              WHEN ping_date
                BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
                  AND DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
              THEN avg_instance_user_count ELSE NULL END)                 AS minus_4_avg_instance_user_count

FROM ping_by_host_quarter
GROUP BY 1
HAVING minus_1_ping_count > 0
    OR minus_2_ping_count > 0
    OR minus_3_ping_count > 0
    OR minus_4_ping_count > 0
    OR current_ping_count > 0
 
-----------------------------------------------------------------------
---> NF: Delta ARR 
  
), delta_arr AS (

SELECT
   dim_parent_crm_account_id                AS account_id,
  
   MAX(CASE 
    WHEN arr_month = DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_arr  ELSE 0 END)                  AS current_delta_arr,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_arr  ELSE 0 END)                 AS minus_1_delta_arr,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_arr  ELSE 0 END)                  AS minus_2_delta_arr,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_arr   ELSE 0 END   )               AS minus_3_delta_arr,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_arr  ELSE 0 END )                 AS minus_4_delta_arr,
  
  
   sum(CASE 
    WHEN arr_month 
      BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN seat_change_arr ELSE 0 END)                 AS current_seat_change_arr,
sum(CASE 
    WHEN arr_month
      BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN seat_change_arr ELSE 0 END)                 AS minus_1_seat_change_arr,
sum(CASE 
    WHEN arr_month 
      BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN seat_change_arr ELSE 0 END)                 AS minus_2_seat_change_arr,
sum(CASE 
    WHEN arr_month
      BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN seat_change_arr ELSE 0 END)                 AS minus_3_seat_change_arr,
 sum(CASE 
    WHEN arr_month
      BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN seat_change_arr ELSE 0 END)                 AS minus_4_seat_change_arr,
  
   sum(CASE 
    WHEN arr_month 
      BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN price_change_arr ELSE 0 END)                 AS current_price_change_arr,
sum(CASE 
    WHEN arr_month
      BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN price_change_arr ELSE 0 END)                 AS minus_1_price_change_arr,
sum(CASE 
    WHEN arr_month 
      BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN price_change_arr ELSE 0 END)                 AS minus_2_price_change_arr,
sum(CASE 
    WHEN arr_month
      BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN price_change_arr ELSE 0 END)                 AS minus_3_price_change_arr,
 sum(CASE 
    WHEN arr_month
      BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN price_change_arr ELSE 0 END)                 AS minus_4_price_change_arr,
  
 sum(CASE 
    WHEN arr_month 
      BETWEEN DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN tier_change_arr ELSE 0 END)                 AS current_tier_change_arr,
sum(CASE 
    WHEN arr_month
      BETWEEN DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN tier_change_arr ELSE 0 END)                 AS minus_1_tier_change_arr,
sum(CASE 
    WHEN arr_month 
      BETWEEN DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN tier_change_arr ELSE 0 END)                 AS minus_2_tier_change_arr,
sum(CASE 
    WHEN arr_month
      BETWEEN DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN tier_change_arr ELSE 0 END)                 AS minus_3_tier_change_arr,
 sum(CASE 
    WHEN arr_month
      BETWEEN DATEADD(year,-5,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE))) 
        AND DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN tier_change_arr ELSE 0 END)                 AS minus_4_tier_change_arr,
  
  MAX(CASE 
    WHEN arr_month = DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN ARRAY_TO_STRING(product_tier_name,',') ELSE '' END)                 AS current_product,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN ARRAY_TO_STRING(product_tier_name,',') ELSE '' END)                 AS minus_1_product,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN ARRAY_TO_STRING(product_tier_name,',') ELSE '' END)                 AS minus_2_product,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN ARRAY_TO_STRING(product_tier_name,',') ELSE '' END)                 AS minus_3_product,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN ARRAY_TO_STRING(product_tier_name,',') ELSE '' END)                 AS minus_4_product,
  
    MAX(CASE 
    WHEN arr_month = DATEADD(year,0,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_quantity ELSE 0 END)                 AS current_number_seats,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-1,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_quantity ELSE 0 END)                 AS minus_1_number_seats,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-2,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_quantity ELSE 0 END)                 AS minus_2_number_seats,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-3,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_quantity ELSE 0 END)                 AS minus_3_number_seats,
  MAX(CASE 
    WHEN arr_month = DATEADD(year,-4,DATEADD(month,-1,DATE_TRUNC(month, CURRENT_DATE)))     
    THEN end_quantity ELSE 0 END)                 AS minus_4_number_seats,
  
  max(ARRAY_TO_STRING(product_delivery_type,','))                                   AS product_delivery_type
FROM  legacy.mart_delta_arr_parent_month
  GROUP BY 1
  
  
-----------------------------------------------------------------------
------- INSERT NEW FEATURES ABOVE HERE
-----------------------------------------------------------------------  
), arr_per_periods AS (
  SELECT upa.account_id,
    upa.parent_crm_account_sales_segment,
    cp.*,
    coalesce(current_p.arr,0)     AS current_arr,
    coalesce(minus_1p.arr,0)      AS minus_1p_arr,
    coalesce(minus_2p.arr,0)      AS minus_2p_arr,
    coalesce(minus_3p.arr,0)      AS minus_3p_arr,
    coalesce(minus_4p.arr,0)      AS minus_4p_arr,
  
    coalesce(current_created_opties,0) AS current_created_opties,
    coalesce(minus_1_created_opties,0) AS minus_1_created_opties,
    coalesce(minus_2_created_opties,0) AS minus_2_created_opties,
    coalesce(minus_3_created_opties,0) AS minus_3_created_opties,
    coalesce(minus_4_created_opties,0) AS minus_4_created_opties,

    coalesce(current_won_opties,0) AS current_won_opties,
    coalesce(minus_1_won_opties,0) AS minus_1_won_opties,
    coalesce(minus_2_won_opties,0) AS minus_2_won_opties,
    coalesce(minus_3_won_opties,0) AS minus_3_won_opties,
  
    coalesce(current_lost_opties,0) AS current_lost_opties,
    coalesce(minus_1_lost_opties,0) AS minus_1_lost_opties,
    coalesce(minus_2_lost_opties,0) AS minus_2_lost_opties,
    coalesce(minus_3_lost_opties,0) AS minus_3_lost_opties,
  
    coalesce(contact.current_created_contacts,0) AS current_created_contacts,
    coalesce(contact.minus_1_created_contacts,0) AS minus_1_created_contacts,
    coalesce(contact.minus_2_created_contacts,0) AS minus_2_created_contacts,
    coalesce(contact.minus_3_created_contacts,0) AS minus_3_created_contacts,
    coalesce(contact.minus_4_created_contacts,0) AS minus_4_created_contacts,
  
    coalesce(tickets.current_created_support_tickets,0) AS current_created_support_tickets,
    coalesce(tickets.minus_1_created_support_tickets,0) AS minus_1_created_support_tickets,
    coalesce(tickets.minus_2_created_support_tickets,0) AS minus_2_created_support_tickets,
    coalesce(tickets.minus_3_created_support_tickets,0) AS minus_3_created_support_tickets,
    coalesce(tickets.minus_4_created_support_tickets,0) AS minus_4_created_support_tickets,
  
    coalesce(tasks.current_sfdc_tasks,0) AS current_sfdc_tasks,
    coalesce(tasks.minus_1_sfdc_tasks,0) AS minus_1_sfdc_tasks,
    coalesce(tasks.minus_2_sfdc_tasks,0) AS minus_2_sfdc_tasks,
    coalesce(tasks.minus_3_sfdc_tasks,0) AS minus_3_sfdc_tasks,
    coalesce(tasks.minus_4_sfdc_tasks,0) AS minus_4_sfdc_tasks,
  
    coalesce(biz_touch.current_biz_touchpoints,0) AS current_biz_touchpoints,
    coalesce(biz_touch.minus_1_biz_touchpoints,0) AS minus_1_biz_touchpoints,
    coalesce(biz_touch.minus_2_biz_touchpoints,0) AS minus_2_biz_touchpoints,
    coalesce(biz_touch.minus_3_biz_touchpoints,0) AS minus_3_biz_touchpoints,
    coalesce(biz_touch.minus_4_biz_touchpoints,0) AS minus_4_biz_touchpoints,
  
    coalesce(ping.current_ping_count,0) AS current_ping_count,
    coalesce(ping.minus_1_ping_count,0) AS minus_1_ping_count,
    coalesce(ping.minus_2_ping_count,0) AS minus_2_ping_count,
    coalesce(ping.minus_3_ping_count,0) AS minus_3_ping_count,
    coalesce(ping.minus_4_ping_count,0) AS minus_4_ping_count,
  
    coalesce(ping.current_avg_instance_user_count,0) AS current_avg_instance_user_count,
    coalesce(ping.minus_1_avg_instance_user_count,0) AS minus_1_avg_instance_user_count,
    coalesce(ping.minus_2_avg_instance_user_count,0) AS minus_2_avg_instance_user_count,
    coalesce(ping.minus_3_avg_instance_user_count,0) AS minus_3_avg_instance_user_count,
    coalesce(ping.minus_4_avg_instance_user_count,0) AS minus_4_avg_instance_user_count,
  
   --delta_arr arr 
    --coalesce(minus_4_delta_arr,0) AS minus_4_delta_arr,
    --coalesce(minus_3_delta_arr,0) AS minus_3_delta_arr,
    --coalesce(minus_2_delta_arr,0) AS minus_2_delta_arr,
    --coalesce(minus_1_delta_arr,0) AS minus_1_delta_arr,
    --coalesce(current_delta_arr,0) AS current_delta_arr,

    coalesce(minus_4_seat_change_arr,0) AS minus_4_seat_change_arr,  
    coalesce(minus_3_seat_change_arr,0) AS minus_3_seat_change_arr,  
    coalesce(minus_2_seat_change_arr,0) AS minus_2_seat_change_arr,
    coalesce(minus_1_seat_change_arr,0) AS minus_1_seat_change_arr,
    coalesce(current_seat_change_arr,0) AS current_seat_change_arr,

    coalesce(minus_4_price_change_arr,0) AS minus_4_price_change_arr,
    coalesce(minus_3_price_change_arr,0) AS minus_3_price_change_arr,
    coalesce(minus_2_price_change_arr,0) AS minus_2_price_change_arr,
    coalesce(minus_1_price_change_arr,0) AS minus_1_price_change_arr,
    coalesce(current_price_change_arr,0) AS current_price_change_arr,
    
    coalesce(minus_4_tier_change_arr,0) AS minus_4_tier_change_arr, 
    coalesce(minus_3_tier_change_arr,0) AS minus_3_tier_change_arr, 
    coalesce(minus_2_tier_change_arr,0) AS minus_2_tier_change_arr,
    coalesce(minus_1_tier_change_arr,0) AS minus_1_tier_change_arr,
    coalesce(current_tier_change_arr,0) AS current_tier_change_arr,

    coalesce(minus_4_number_seats,0) AS minus_4_number_seats,
    coalesce(minus_3_number_seats,0) AS minus_3_number_seats,
    coalesce(minus_2_number_seats,0) AS minus_2_number_seats,
    coalesce(minus_1_number_seats,0) AS minus_1_number_seats,
    coalesce(current_number_seats,0) AS current_number_seats,
  
    coalesce(minus_4_product,'None') AS minus_4_product,
    coalesce(minus_3_product,'None') AS minus_3_product,
    coalesce(minus_2_product,'None') AS minus_2_product,
    coalesce(minus_1_product,'None') AS minus_1_product,
    coalesce(current_product,'None') AS current_product,
  
    COALESCE(product_delivery_type,'Unknown')  AS product_delivery_type,
    COALESCE(upa_industry,'Unknown') as upa_industry
    
 FROM upa
    LEFT JOIN opty_summary ops
      ON ops.account_id = upa.account_id
    LEFT JOIN contact_summary contact
      ON contact.account_id = upa.account_id
    LEFT JOIN zendesk_summary tickets
      ON tickets.account_id = upa.account_id
    LEFT JOIN tasks_summary tasks
      ON tasks.account_id = upa.account_id
    LEFT JOIN mkt_touch_summary biz_touch
      ON biz_touch.account_id = upa.account_id
    LEFT JOIN ping_summary ping
      ON ping.account_id = upa.account_id
    LEFT JOIN delta_arr
      ON delta_arr.account_id = upa.account_id
    CROSS JOIN target_periods cp
      LEFT JOIN agg_arr_month current_p 
      ON cp.current_month = current_p.arr_month
      AND upa.account_id = current_p.account_id
    LEFT JOIN agg_arr_month minus_1p
      ON minus_1p.account_id = upa.account_id
      AND minus_1p.arr_month = cp.minus_1_year_month
    LEFT JOIN agg_arr_month minus_2p
      ON minus_2p.account_id = upa.account_id
      AND minus_2p.arr_month = cp.minus_2_year_month
    LEFT JOIN agg_arr_month minus_3p
      ON minus_3p.account_id = upa.account_id
      AND minus_3p.arr_month = cp.minus_3_year_month
    LEFT JOIN agg_arr_month minus_4p
      ON minus_4p.account_id = upa.account_id
      AND minus_4p.arr_month = cp.minus_4_year_month
    

WHERE (minus_1p.arr IS NOT NULL
        OR minus_2p.arr_month IS NOT NULL
        OR minus_3p.arr_month IS NOT NULL
        OR minus_4p.arr_month IS NOT NULL)
)
